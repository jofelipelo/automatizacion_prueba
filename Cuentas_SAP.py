# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 09:20:41 2021

@author: joortiz
"""


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
from getpass import getpass
from datetime import date
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta
import glob
import os
import pyautogui
import clipboard

ruta_descargas = r'D:\joortiz\Downloads'
ruta_validacion = r'N:\Administracion RO\9.Conciliación contable PO\2.Conciliación contable'
#username = input("Ingresa tu nombre de usuario: ")
#password = getpass("Ingresa tu contraseña: ")

DRIVER_PATH = 'D:/joortiz/Downloads/chromedriver_win32/chromedriver'

def SAP_Abrir(DRIVER_PATH):
    #Abre el aplicativo de SAP Gui ERP
    if date.today().month == 1:
        fecha_inicio = (date.today() - relativedelta(years=1)).replace(day=1)
    else:
        fecha_inicio = date.today().replace(month=1).replace(day=1)
    fecha_fin = date.today().replace(day=1) - relativedelta(days=1)
    
    fecha_inicio_SAP = fecha_inicio.strftime('%d') + '.' + fecha_inicio.strftime('%m') + '.' + fecha_inicio.strftime('%Y') 
    fecha_fin_SAP = fecha_fin.strftime('%d') + '.' + fecha_fin.strftime('%m') + '.' + fecha_fin.strftime('%Y')

    
    df1 = pd.DataFrame({'Compañias':[1000,1200,3000,4000,5000,8000,9000,8100,500,700,300]})
    df2 = pd.read_excel(ruta_validacion + '\Validación Contable - Nuevo Prototipo.xlsx')
    tabs = []
    for i in range(24):
        tabs = np.append(tabs, Keys.TAB)
    tabs = np.append(tabs, Keys.ENTER)
    driver = webdriver.Chrome(executable_path=DRIVER_PATH)
    driver.get("http://intranet.bancolombia.corp/Contador/LinkSAP_21112014.html")
    #WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.ID, "logonuidfield")))
    #login = driver.find_element_by_id("logonuidfield").send_keys(username)
    #password2 = driver.find_element_by_id("logonpassfield").send_keys(password)
    #driver.find_element_by_id("logonpassfield").send_keys(Keys.ENTER)
    time.sleep(5)
    WebElement = driver.find_element_by_xpath("/html/body")
    WebElement.send_keys(tabs)
    time.sleep(2)
    list_of_files = glob.glob(ruta_descargas + '\*.sap')
    latest_file = max(list_of_files, key=os.path.getctime)
    os.startfile(latest_file)
    time.sleep(2)
    pyautogui.press(["tab", "tab", "tab", "enter"])
    time.sleep(5)
    pyautogui.typewrite('FAGLL03')
    pyautogui.press(["enter"])
    time.sleep(1)
    pyautogui.press(["tab", "tab", "tab"])
    pyautogui.typewrite("1000")
    pyautogui.press("tab")
    pyautogui.typewrite("1000")
    pyautogui.press("tab")
    pyautogui.press("enter")    
    time.sleep(1)
    df1.iloc[:,0].to_clipboard(sep='\n', index=False)
    pyautogui.hotkey('shift', 'f12')
    time.sleep(1)
    pyautogui.hotkey('f8')
    time.sleep(1)
    pyautogui.hotkey('shift', 'tab')
    pyautogui.hotkey('shift', 'tab')
    pyautogui.press("enter")
    time.sleep(1)
    df2.iloc[:,1].to_clipboard(sep='\n', index=False)
    pyautogui.hotkey('shift', 'f12')
    time.sleep(1)
    pyautogui.hotkey('f8')
    time.sleep(1)
    pyautogui.press(["tab", "tab", "tab", "tab", "tab", "tab", "down", "down", "tab", "tab", "tab"])
    pyautogui.write(fecha_inicio_SAP)
    pyautogui.press("tab")
    pyautogui.write(fecha_fin_SAP)
    pyautogui.press(["tab", "tab"])
    pyautogui.typewrite("/RIESGVALCDC")
    pyautogui.hotkey('f9')
    
    #### Nuevos cambios

    pyautogui.write(fecha_fin_SAP)
    pyautogui.press(["tab", "tab"])
    pyautogui.typewrite("/RIESGVALCDC")
    pyautogui.hotkey('f8')
    
    #### Nuevos cambios

    pyautogui.write(fecha_fin_SAP)
    pyautogui.press(["tab", "tab"])
    pyautogui.typewrite("/RIESGVALCDC")
    pyautogui.hotkey('f9')

    

    
    

